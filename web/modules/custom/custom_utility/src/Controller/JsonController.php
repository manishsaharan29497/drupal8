<?php

namespace Drupal\custom_utility\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class JsonController.
 *
 * Return JSON Object Controller.
 */
class JsonController extends ControllerBase {

  /**
   * JSON Object.
   *
   * @param string $siteapikey
   *   SiteApiKey to validate from url.
   * @param int $nid
   *   Node ID.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   JSON Object of node.
   */
  public function jsonObject($siteapikey, $nid) {
    $node = Node::load($nid);
    if (!empty($node)) {
      $output = [
        'nid' => $node->id(),
        'title' => $node->getTitle(),
        'type' => $node->getType(),
        'data' => $node->body->value,
      ];

      return new JsonResponse($output);
    }
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param string $siteapikey
   *   Site Api Key to validate from url.
   * @param int $nid
   *   Node ID.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access($siteapikey, $nid, AccountInterface $account) {
    return AccessResult::allowedIf($this->customCondition($siteapikey, $nid, $account));
  }

  /**
   * Custom Condition to validate route.
   */
  public function customCondition($siteapikey, $nid, $account) {
    $originalkey = \Drupal::config('system.site')->get('siteapikey');
    $node = Node::load($nid);
    if ($siteapikey == $originalkey && !empty($node) && $node->getType() == 'page' && $account->isAuthenticated()) {
      return TRUE;
    }
    return FALSE;
  }

}
